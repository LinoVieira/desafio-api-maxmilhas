package br.com.steps;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class userLoginController {
	
	private Response response;
	private  RequestSpecification request;

	//setup the endpoint path
	@Given("^The user login endpoint is already configured$")
	public void the_user_access_endpoint_is_already_configured() throws Throwable {
		RestAssured.baseURI = "https://reqres.in/api/";
		RestAssured.basePath = "/login/";
		this.request = RestAssured.given();
		this.response = this.request.when().get().then().extract().response();
	}
	
	//login with valid user
	@When("^I do a request to login a user with email \"([^\"]*)\" and password \"([^\"]*)\" the user register endpoint$")
	public void i_do_a_request_to_login_a_user_with_email_and_password_the_user_register_endpoint(String email, String password) throws Throwable {
		String payload = "{\n" +
		        "  \"email\": \""+email+"\",\n" +
		        "  \"password\": \""+password+"\"}";
		response = request.contentType(ContentType.JSON).body(payload).post("");
		String message = this.response.<String>path("token");
		Assert.assertEquals(message, "QpwL5tke4Pnpja7X4");
		System.out.println(response.asString());
	}
	
	//status code assert
	@Then("^The status code is \"([^\"]*)\"$")
	public void the_status_code_is(String statusCode) throws Throwable {
		this.response.then().statusCode(Integer.parseInt(statusCode));
	}
	
	// login with an invalid user
	@When("^I do a request to login an invalid user with email \"([^\"]*)\" and password \"([^\"]*)\" the user register endpoint$")
	public void i_do_a_request_to_login_an_invalid_user_with_email_and_password_the_user_register_endpoint(String email, String password) throws Throwable {
		String payload = "{\n" +
		        "  \"email\": \""+email+"\",\n" +
		        "  \"password\": \""+password+"\"}";
		response = request.contentType(ContentType.JSON).body(payload).post("");
		String message = this.response.<String>path("error");
		Assert.assertEquals(message, "Missing password");
		System.out.println(response.asString());
	}
	

}
