package br.com.steps;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class userController {

	private Response response;
	private RequestSpecification request;

	private int userCount;
	

	//setup the endpoint path
	@Given("^The user endpoint is already configured$")
	public void the_endpoint_is_already_configured() throws Throwable {
		RestAssured.baseURI = "https://reqres.in/api/";
		RestAssured.basePath = "/users/";
		this.request = RestAssured.given();
		this.response = this.request.when().get().then().extract().response();
	}

////Get all users from user API	
	@When("^I do a GET request for all users on the user endpoint$")
	public void i_do_a_GET_request_for_all_users_on_the_user_endpoint() throws Throwable {
		Integer currentPage = this.response.<Integer>path("page");
		Integer totalPages = this.response.<Integer>path("total_pages");
		//loop for to get all users from all pages
		for(int i = currentPage; i <= totalPages; i++) {
		  Response response = this.request.queryParam("page", i).when().get().then().extract().response();
		  List user = response.path("data");
		  this.userCount += user.size();
		}
	}
	
	//Asserting the number of all user
	@Then("^The total users should be equal to the total on the request header$")
	public void the_total_users_should_be_equal_to_the_total_on_the_request_header() throws Throwable {
		Integer total = this.response.<Integer>path("total");
		Assert.assertTrue(total == this.userCount);
		
	}
	//validating the status code response
	@Then("^I should have the status code \"([^\"]*)\"$")
	public void i_should_have_the_status_code(String statusCode) throws Throwable {
		this.response.then().statusCode(Integer.parseInt(statusCode));

	}
	//getting a single user from the user controller API
	@When("^I do a GET request for a single user \"([^\"]*)\" on the user endpoint$")
	public void i_do_a_GET_request_for_a_single_user_on_the_user_endpoint(String id) throws Throwable {
		 this.response = this.request.when().get(id).then().extract().response();
		 
	}
	//getting the user selected and asserting it
	@Then("^I should get all the user data$")
	public void i_should_get_all_the_user_data() throws Throwable {
		String resolve = this.response.getBody().asString();
		JSONObject data = new JSONObject(resolve).getJSONObject("data");
		Assert.assertEquals(data.get("email"), "janet.weaver@reqres.in");
		String userData = this.request.when().get("2").then().extract().response().asString();
		System.out.println(userData);
	}
	
	/*Adding new users*/
	@When("^I do a POST request to insert a new single user with name \"([^\"]*)\" and job \"([^\"]*)\" on the user endpoint$")
	public void i_do_a_POST_request_to_insert_a_new_single_user_with_name_and_job_on_the_user_endpoint(String name, String job) throws Throwable {
		String payload = "{\n" +
		        "  \"name\": \""+name+"\",\n" +
		        "  \"job\": \""+job+"\"}";
		response = request.contentType(ContentType.JSON).body(payload).post("");
		System.out.println(response.asString());
	}
	
	/*user update*/
	@When("^I do a PUT request to insert a new single user with name \"([^\"]*)\" and job \"([^\"]*)\" on the user endpoint$")
	public void i_do_a_PUT_request_to_insert_a_new_single_user_with_name_and_job_on_the_user_endpoint(String name, String job) throws Throwable {
		String payload = "{\n" +
		        "  \"name\": \""+name+"\",\n" +
		        "  \"job\": \""+job+"\"}";
		response = request.contentType(ContentType.JSON).body(payload).put("");
		System.out.println(response.asString());
	}
	/*Pacth request*/
	@When("^I do a PATCH request to insert a new single user with name \"([^\"]*)\" and job \"([^\"]*)\" on the user endpoint$")
	public void i_do_a_PATCH_request_to_insert_a_new_single_user_with_name_and_job_on_the_user_endpoint(String name, String job) throws Throwable {
		String payload = "{\n" +
		        "  \"name\": \""+name+"\",\n" +
		        "  \"job\": \""+job+"\"}";
		response = request.contentType(ContentType.JSON).body(payload).patch("");
		System.out.println(response.asString());
	}
	// DELETE request
	@When("^I do a DELETE request to delete a single user with name \"([^\"]*)\" and job \"([^\"]*)\" on the user endpoint$")
	public void i_do_a_DELETE_request_to_delete_a_single_user_with_name_and_job_on_the_user_endpoint(String name, String job) throws Throwable {
		String payload = "{\n" +
		        "  \"name\": \""+name+"\",\n" +
		        "  \"job\": \""+job+"\"}";
		response = request.contentType(ContentType.JSON).body(payload).delete("");
		System.out.println(response.asString());
	}
	
}
