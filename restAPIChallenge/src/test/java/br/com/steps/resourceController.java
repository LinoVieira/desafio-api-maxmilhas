package br.com.steps;

import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class resourceController {
	
	private Response response;
	private  RequestSpecification request;

	private int resourceCount;
	
	
	@Given("^The resource endpoint is already configured$")
	public void the_endpoint_is_already_configured() throws Throwable {
		RestAssured.baseURI = "https://reqres.in/api/";
		RestAssured.basePath = "/unknown/";
		this.request = RestAssured.given();
		this.response = this.request.when().get().then().extract().response();
	}

	@When("^I do request to GET all resources on the resource endpoint$")
	public void i_do_request_to_GET_all_resources_on_the_resource_endpoint() throws Throwable {
		Integer currentPage = this.response.<Integer>path("page");
		Integer totalPages = this.response.<Integer>path("total_pages");
		//loop for to get all users from all pages
		for(int i = currentPage; i <= totalPages; i++) {
		  Response response = this.request.queryParam("page", i).when().get().then().extract().response();
		  List resource = response.path("data");
		  this.resourceCount += resource.size();
		}
	}

	@Then("^The total resources should be equal to the total on the request header$")
	public void the_total_resources_should_be_equal_to_the_total_on_the_request_header() throws Throwable {
		Integer total = this.response.<Integer>path("total");
		Assert.assertTrue(total == this.resourceCount);
	}
	
	@When("^I do a GET request for a single resource \"([^\"]*)\" on the resource endpoint$")
	public void i_do_a_GET_request_for_a_single_resource_on_the_resource_endpoint(String id) throws Throwable {
		this.response = this.request.when().get(id).then().extract().response();
	}
	
	@Then("^I should get all the resource data$")
	public void i_should_get_all_the_resource_data() throws Throwable {
		String resolve = this.response.getBody().asString();
		JSONObject data = new JSONObject(resolve).getJSONObject("data");
		Assert.assertEquals(data.get("color"), "#98B2D1");
		String resourceData = this.request.when().get("1").then().extract().response().asString();
		
		System.out.println(resourceData);
	}
	@Then("^Resource status code is \"([^\"]*)\"$")
	public void resource_status_code_is(String statusCode) throws Throwable {
		this.response.then().statusCode(Integer.parseInt(statusCode));
	}

}
