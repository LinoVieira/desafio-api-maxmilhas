package br.com.steps;

import org.json.JSONObject;
import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class userRegisterController {
	private Response response;
	private  RequestSpecification request;

	//Endpoint setup
	@Given("^The user access endpoint is already configured$")
	public void the_user_access_endpoint_is_already_configured() throws Throwable {
		RestAssured.baseURI = "https://reqres.in/api/";
		RestAssured.basePath = "/register/";
		this.request = RestAssured.given();
		this.response = this.request.when().get().then().extract().response();
	}
	
	//User register
	@When("^I do a request to register a user with email \"([^\"]*)\" and password \"([^\"]*)\" the user register endpoint$")
	public void i_do_a_request_to_register_a_user_with_email_and_the_user_register_endpoint(String email, String password) throws Throwable {
		String payload = "{\n" +
		        "  \"email\": \""+email+"\",\n" +
		        "  \"password\": \""+password+"\"}";
		response = request.contentType(ContentType.JSON).body(payload).post("");
		System.out.println(response.asString());
	}
	
	//asserting the status code
	@Then("^The status code should be \"([^\"]*)\"$")
	public void the_status_code_should_be(String statusCode) throws Throwable {
		this.response.then().statusCode(Integer.parseInt(statusCode));
	}

	//BAD request for a user register
	@When("^I do BAD request to register a user with email \"([^\"]*)\" and password \"([^\"]*)\" the user register endpoint$")
	public void i_do_BAD_request_to_register_a_user_with_email_and_password_the_user_register_endpoint(String email, String password) throws Throwable {
		String payload = "{\n" +
		        "  \"email\": \""+email+"\",\n" +
		        "  \"password\": \""+password+"\"}";
		response = request.contentType(ContentType.JSON).body(payload).post("");
		String message = this.response.<String>path("error");
		Assert.assertEquals(message, "Missing password");
		System.out.println(response.asString());
	}




}
