Feature: Resource controller API

  As a tester I want to validate the resource controller API requests

# the resource endpoint name it's as unknow
@run
  Scenario Outline: Success request to GET all resources
   Given The resource endpoint is already configured
	    When I do request to GET all resources on the resource endpoint
	    Then Resource status code is <statusCode>      
	    And The total resources should be equal to the total on the request header
	   Examples:
	    |statusCode|
		 	|"200"     |

@run 
	Scenario Outline: Success request to GET a single resource
	 Given The resource endpoint is already configured
		When I do a GET request for a single resource <id> on the resource endpoint
		Then I should get all the resource data
		And Resource status code is <statusCode>
Examples:
|id  |statusCode|
|"1" |"200"     |

@run 
	Scenario Outline: BAD request to GET a single resource
	  Given The resource endpoint is already configured
		When I do a GET request for a single resource <id> on the resource endpoint
		Then Resource status code is <statusCode>
Examples:
|id  |statusCode|
|"23" |"404"     |
