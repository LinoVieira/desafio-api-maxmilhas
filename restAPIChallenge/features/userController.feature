Feature: User controller API

	As a tester I want to validate the user API controller requests

@run
  Scenario: Success request to GET all users
  	  Given The user endpoint is already configured 
	    When I do a GET request for all users on the user endpoint
	    Then The total users should be equal to the total on the request header 
	    And I should have the status code "200"
    
@run 
	Scenario Outline: Success request to GET a single user
	  Given The user endpoint is already configured
		When I do a GET request for a single user <id> on the user endpoint
		Then I should get all the user data
		And I should have the status code <statusCode>
Examples:
|id  |statusCode|
|"2" |"200"     |

@run 
	Scenario Outline: BAD request to GET a single user
	  Given The user endpoint is already configured
		When I do a GET request for a single user <id> on the user endpoint
		Then I should have the status code <statusCode>
Examples:
|id  |statusCode|
|"23" |"404"     |

@run 
	Scenario: Success POST request to insert a single user
	  Given The user endpoint is already configured
		When I do a POST request to insert a new single user with name "Joao" and job "Developer" on the user endpoint
		Then I should have the status code "201"


 #On this scenarios was able only to validate the status code of the request, its not able
 #uptade a iserted user on this api  
 #the user id create after the post is randoming and is not possible to updated it
@run 	
	Scenario: PUT request to update a single user
	  Given The user endpoint is already configured
		When I do a PUT request to insert a new single user with name "Joao Mario" and job "Manager" on the user endpoint
		Then I should have the status code "200"

@run 
	Scenario: PATCH request to update a single user
	  Given The user endpoint is already configured
		When I do a PATCH request to insert a new single user with name "Joao Mario" and job "Manager" on the user endpoint
		Then I should have the status code "200"

@run 
	Scenario: DELETE request to delete a single user
	  Given The user endpoint is already configured
		When I do a DELETE request to delete a single user with name "Joao Mario" and job "Manager" on the user endpoint
		Then I should have the status code "204"
