Feature: User access controller API
  As a tester I want to validate the user register and login requests
 
@run
Scenario: Success user register request
	  Given The user access endpoint is already configured
		When I do a request to register a user with email "eve.holt@reqres.in" and password "pistol" the user register endpoint
		Then The status code should be "200"

@run
Scenario: BAD user register request
	  Given The user access endpoint is already configured
		When I do BAD request to register a user with email "baduser@.com" and password "" the user register endpoint
		Then The status code should be "400"



