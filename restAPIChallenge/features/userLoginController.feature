Feature: User Login controller API
  As a tester I want to valudate the user login requests

@run
Scenario: Success user login
	  Given The user login endpoint is already configured
		When I do a request to login a user with email "eve.holt@reqres.in" and password "cityslicka" the user register endpoint
		Then The status code is "200"
		
@run
Scenario: Unsuccess user login
	  Given The user login endpoint is already configured
		When I do a request to login an invalid user with email "eve.holt@reqres.in" and password "" the user register endpoint
		Then The status code is "400"
		
		


